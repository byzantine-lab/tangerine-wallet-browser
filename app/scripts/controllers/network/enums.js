const ROPSTEN = 'ropsten'
const RINKEBY = 'rinkeby'
const KOVAN = 'kovan'
const MAINNET = 'mainnet'
const LOCALHOST = 'localhost'
const GOERLI = 'goerli'

const TANGERINE_TESTNET = 'tangerine_testnet'

const MAINNET_CODE = 1
const ROPSTEN_CODE = 3
const RINKEYBY_CODE = 4
const KOVAN_CODE = 42
const GOERLI_CODE = 5

const TANGERINE_TESTNET_CODE = 374

const ROPSTEN_DISPLAY_NAME = 'Ropsten'
const RINKEBY_DISPLAY_NAME = 'Rinkeby'
const KOVAN_DISPLAY_NAME = 'Kovan'
const MAINNET_DISPLAY_NAME = 'Main Ethereum Network'
const GOERLI_DISPLAY_NAME = 'Goerli'

const TANGERINE_TESTNET_DISPLAYNAME = 'Tangerine Testnet Displayname'
const TANGERINE_TESTNET_RPC = 'https://testnet-rpc.tangerine-network.io'

module.exports = {
  ROPSTEN,
  RINKEBY,
  KOVAN,
  MAINNET,
  LOCALHOST,
  GOERLI,
  MAINNET_CODE,
  ROPSTEN_CODE,
  RINKEYBY_CODE,
  KOVAN_CODE,
  GOERLI_CODE,
  ROPSTEN_DISPLAY_NAME,
  RINKEBY_DISPLAY_NAME,
  KOVAN_DISPLAY_NAME,
  MAINNET_DISPLAY_NAME,
  GOERLI_DISPLAY_NAME,
  // Tangerine Network
  TANGERINE_TESTNET,
  TANGERINE_TESTNET_CODE,
  TANGERINE_TESTNET_DISPLAYNAME,
  TANGERINE_TESTNET_RPC,
}
